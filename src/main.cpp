#include <iostream>
#include <string>
#include "pessoa.hpp"
#include "aluno.hpp"
#include "professor.hpp"

#include <vector>

using namespace std;

// Templates

template <typename T1>
T1 getInput(){
    while(true){
        T1 valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";            
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }    
}

string getString(){
    while(true){
        string valor;
        try{
            getline(cin, valor);    
        }
        catch(const std::ios_base::failure& e){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida - " << e.what() << " - Insira novamente: " << endl;
        }
        return valor;
    }
}
/*
int getInt(){
    while(true){
        int valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";            
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}
long getLong(){
    while(true){
        long valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";            
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}
float getFloat(){
    while(true){
        float valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";            
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}
string getString(){
    while(true){
        string valor;
        cin >> valor;
        if(cin.fail()){
            cin.clear();
            cin.ignore(32767,'\n');
            cout << "Entrada inválida. Insira novamente: ";            
        }
        else{
            cin.ignore(32767,'\n');
            return valor;
        }
    }
}
*/
int main(int argc, char const *argv[])
{
    
    vector< Pessoa * > lista_de_pessoas;


    // Menu
    while(true){
        system("clear");
        cout << "Cadastro da Universidade" << endl;
        cout << "------------------------------------" << endl;
        cout << "1. Adicionar aluno" << endl;
        cout << "2. Adicionar professor" << endl;
        cout << "3. Remover item" << endl;
        cout << "4. Imprimir lista" << endl;
        cout << "0. Sair do sistema" << endl;
        cout << "------------------------------------" << endl;

        //int opcao = getInt();
        int opcao = getInput<int>();
        //cin >> opcao;

        string nome;
        long cpf;
        string telefone;
        float salario;
        string formacao;
        long matricula;
        string curso;

        switch(opcao){
            case 1:
                system("clear");
                cout << "Dados do aluno:" << endl;
                cout << "Nome: ";
                //cin >> nome;
                nome = getString();
                cout << "CPF: ";
                //cin >> cpf;
                cpf = getInput<long>();//getLong();
                cout << "Telefone: ";
                //cin >> telefone;
                telefone = getString();
                cout << "Matrícula: ";
                //cin >> matricula;
                matricula = getInput<long>();//getLong();
                cout << "Curso: ";
                //cin >> curso;
                curso = getString();

                lista_de_pessoas.push_back(new Aluno(nome, telefone, cpf, matricula, curso));
                break;
            case 2:
                system("clear");
                cout << "Dados do professor:" << endl;
                cout << "Nome: ";
                //cin >> nome;
                nome = getString();
                cout << "CPF: ";
                //cin >> cpf;
                cpf = getInput<long>();
                cout << "Telefone: ";
                //cin >> telefone; 
                telefone = getString();
                cout << "Salário: ";
                //cin >> salario;
                salario = getInput<float>();
                cout << "Formação: ";
                //cin >> formacao; 
                formacao = getString();
                
                lista_de_pessoas.push_back(new Professor(nome, telefone, cpf, salario, formacao));
                               
                break;
            case 4:
                system("clear");

                cout << "Lista de pessoas da Universidade" << endl;
                cout << "================================" << endl;
                for (Pessoa * p: lista_de_pessoas)
                {
                    p->imprime_dados();
                    cout << "================================" << endl;
                }
                cin.get();
                break;
            case 0:
                exit(0);
                break;
            default:
                break;


        }


    }


    return 0;
}













